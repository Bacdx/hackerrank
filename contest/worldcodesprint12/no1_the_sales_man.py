#!/bin/python3
import sys


def minimumTime(x):
    l = len(x)
    x.sort()
    minimum = 0
    for i in range(l - 1):
        minimum += x[i + 1] - x[i]
    return minimum


if __name__ == "__main__":
    t = int(input().strip())
    for a0 in range(t):
        n = int(input().strip())
        x = list(map(int, input().strip().split(' ')))
        result = minimumTime(x)
        print(result)
