def findprimefactor(n):
    i = 2
    listprimefactor = []
    while i ** 2 <= n:
        while n % i == 0:
            n = n / i
            listprimefactor.append(i)
        i = i + 1
    if n > 1:
        listprimefactor.append(int(n))
    return listprimefactor


def isprime(a):
    counter = 0
    if a == 1:
        return False
    if a in [2, 3, 5, 7, 11]:
        return True
    if a % 6 not in [1, 5]:
        return False
    b = int(a**0.5) + 1
    c = 6 * counter + 1
    d = 6 * counter + 5
    while c < b or d < b:
        if (c != 1 and a % c == 0) or (a % d == 0):
            return False
        counter += 1
        c = 6 * counter + 1
        d = 6 * counter + 5
    return True


def longestSequence(n):
    def longestperstick(a):
        count = 0
        product = 1
        if isprime(a):
            return a + 1
        primefactors = findprimefactor(a)
        for i in range(len(primefactors) - 1, -1, -1):
            product *= primefactors[i]
            count += product
        return count + 1

    longestturn = 0
    for a in n:
        longestturn += longestperstick(a)
    return longestturn


if __name__ == '__main__':
    n = int(input().strip())
    a = list(map(int, input().strip().split(' ')))
    result = longestSequence(a)
    print(result)
